var express = require('express');
var app = express();
var mmdbreader = require('maxmind-db-reader');
var countries = mmdbreader.openSync('GeoIP2-City.mmdb');
var url = require('url');
var request = require('request');
var openweathermapCallUrl = url.parse('http://api.openweathermap.org/data/2.5/weather?lat=42&lon=29&units=metric&appid=e39af65117399d5f598d072ad61f805f', true);
var openweathermapCityUrl = url.parse('http://api.openweathermap.org/data/2.5/weather?q=istanbul&units=metric&appid=e39af65117399d5f598d072ad61f805f', true);
// var redis = require("redis").createClient("16705","pub-redis-16705.us-east-1-4.3.ec2.garantiadata.com", {no_ready_check: true});
var redis = require("redis").createClient("11432","pub-redis-11432.eu-west-1-1.2.ec2.garantiadata.com", {no_ready_check: true});
var opbeat = require('opbeat').start({
  appId: '1cc0584083',
  organizationId: 'ebd3bf76fd4743e5b23f355553e9e61c',
  secretToken: 'ab6f69e588417910dd7e315a2f1c4a3de0436797'
})

app.use(opbeat.middleware.express())
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});


var cities = {}

app.get( '/status', function( request, response, next ) {
	response.send('OK')
});

app.get( '/loaderio-188d3cab220f0108ead1170c00edab16/', function( request, response, next ) {
	response.send('loaderio-188d3cab220f0108ead1170c00edab16')
});

app.get( '/city/:name', function( request, response, next ) {
	var city = request.params.name.charAt(0).toUpperCase() + request.params.name.slice(1);
  checkCache(city, function (err, res) {
    if (err) return next(err);
    if (res){
     console.log('served from cache');
     response.send(res);
     return next();
    } 
    if (!res) {
		  getWeather(city, function (err, res) {
		    if (err) return next(err);
		    if (!res) return next('weather information not found');
		    console.log('served from openwathermap api');
		    response.send(res);
		    return next();
		  });
    }
  });
});


app.get( '/', function( request, response, next ) {
	var ip = request.headers['x-forwarded-for'] || request.connection.remoteAddress;

  getLocation(ip, function (err, location) {
  	if (err) return next(err)
    checkCache(location, function (err, res) {
      if (err) return next(err);

      if (res){
      	console.log('served from cache');
      	response.send(res);
      	return next();
      } 
      if (!res) {
		    getWeather(location, function (err, res) {
		      if (err) return next(err);
		      if (!res) return next('weather information not found');
		      console.log('served from openwathermap api');
		      response.send(res);
		      return next();
		    });
      }
    });
  });
});

function getLocation (ip, next) {
	var geodata = countries.getGeoDataSync(ip);
	if (!geodata){
		var err = 'Could not find location';
		return next (err);
	}

	if (geodata && geodata.city && geodata.city.names.en)
		return next(null, geodata.city.names.en)

	return next(null, "Istanbul");
};

function checkCache (city, next) {

	if (cities.hasOwnProperty(city)){
		return next (null,cities[city])
	}
		

	return next(null, null)
	// redis.get(city, function (err, result) {
	// 	if (err) return next(err);
	//   return next(null, result);
	// });
};

function getWeather (city, next) {
	// openweathermapCallUrl.query.lat = location.latitude;
	// openweathermapCallUrl.query.lon = location.longitude;
	openweathermapCityUrl.query.q = city;
	delete openweathermapCityUrl.search;
	var requestUrl = url.format(openweathermapCityUrl);
	request(requestUrl, function (error, response, body) {
		if (error) return next (error);
	  if (!error && response.statusCode == 200) {
	  	var info = JSON.parse(body);
	    var weather = {}
	    weather.city = city;

	    if (info.weather && info.weather[0]['id'])
	    	weather.id = info.weather[0]['id'];
	    if (info.main)
	    	weather.main = info.main 
	    if (info.main.temp)
	    	weather.Temperature = info.main.temp;
	    if (info.main.humidity)
	    	weather.Humidity = info.main.humidity;
	    if (info.weather[0].description)
	    	weather.Status = info.weather[0].description;
	    if (info.wind)
	    	weather.wind = info.wind;

	    weather["createdDate"] = new Date();
	    cities[city] = weather;
	    return next (null, weather);
 			// var cacheData = JSON.stringify(weather);
 			// redis.set(city, cacheData);
 			// redis.expire(city, 3600);
 			// return next(null, weather);
	  }
	});
}

var minutes = 5, the_interval = minutes * 60 * 1000;
setInterval(function() {
  console.log("5 minutes key check");
  var arr = Object.keys(cities);
  if (arr.length === 0)
  	return
	for (var i = 0, len = arr.length; i < len; i++) {
	  var now = new Date()
	  keyDate = cities[arr[i]]["createdDate"];
	  var diffMs = (now - keyDate);
	  var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000);
	  console.log('diff',diffMins)
	  if (diffMins > 60){
	  	console.log('key deleted', arr[i])
	  	delete cities[arr[i]]; 
	  }
	  	
	}
  // do your stuff here
}, the_interval);
var port = process.env.PORT || 8080;

app.listen( port, function() {
    console.log( 'Express server listening on port %d in %s mode', port, app.settings.env );
});